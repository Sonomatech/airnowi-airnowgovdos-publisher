package com.sti.airnow.dosdal;

import com.google.common.collect.Lists;
import com.sti.justice.Pair;
import com.sti.justice.dmc.model.AgencyMetaData;
import com.sti.justice.dms.model.Agency;
import com.sti.justice.dms.model.Parameter;
import com.sti.justice.dms.model.Site;
import com.sti.justice.dms.model.SurrogateData;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;


public class PublishRSSFile extends AbstractDosPublisher
{
    public static void main(String[] args) {
        standardMain(args, PublishRSSFile.class);
    }

    public static String getRSSDirectory(Site site)
    {
        final String siteName = sanitizeFilename(site.getName());
        return "RSS/" + siteName;
    }
    
    public static String getRSSFilename(Site site, Parameter p)
    {
        final String siteName = sanitizeFilename(site.getName());
        final String paramName = sanitizeFilename(p.getIngestCode());
        return siteName + "-" + paramName + ".xml";
    }
    
    public static String getRSSPath(Site site, Parameter p)
    {
        return getRSSDirectory(site) + "/" + getRSSFilename(site, p);
    }
    
    @Override
    protected boolean generateOutput()
    {        
        List<Pair<Agency,Site>> sites = retrieveAllSites();
        printOut("Found "+sites.size()+" sites.");
        sites.stream().forEach((pair) -> {  
            final Agency agency = pair.getFirst();
            final Site site = pair.getSecond();
            
            AgencyMetaData dmcAgency = agencyMetaDataDal.getAgencyById(agency.getAgencyId());
            if (dmcAgency == null) {
                printError("Error: No DMC agency record found for agency ID "+agency.getAgencyId() +
                        " for site "+site.getName()+", skipping.");
                return;
            }
            if (!dmcAgency.getShowOnAirNowGov()) {
                // Don't bother generating any CSV files.  There might be some already
                // generated but leaving them there shouldn't hurt anything.
                printOut("Show on AirNow checkbox is off for site "+site.getName()+", skipping.");
                return;
            }

            List<Integer> params = parameterDal.getPrimaryParameterIdsForSite(site.getSiteID());
            printOut("Found "+params.size()+" primary parameters for site "+site.getName()+".");
            params.stream().forEach((pid) -> {
                final Parameter p = parameterDal.getById(pid);
                Integer possiblePoc = getPOC(site.getSiteID(), p.getParameterId());
                if (possiblePoc == null) {
                    printError("Warning: Could not determine POC for site '"+site.getName()+"' and parameter '"+p.getDisplayName()+"'.");
                    return;
                }
                final int poc = possiblePoc;
                List<SurrogateData> data = dataDal.getHourlySurrogates(site.getSiteID(), pid, poc, -24);
                printOut("Found "+data.size()+" hourly values for site "+site.getName()+" and parameter ID "+pid+" and POC "+poc+".");
                
                try {
                    // Sort by time
                    DateTimeComparator datecomp = DateTimeComparator.getInstance();
                    data = data.stream().sorted((d1, d2) -> datecomp.compare(d1.getLocalEndTime(), d2.getLocalEndTime())).collect(Collectors.toList());
                } catch (Exception e) {
                    printError("Error sorting by date for agency '"+agency.getName()+"' and site '"+site.getName()+"'.", e);
                }
                // Now remove the last element (if the last value is missing, but the previous one is not) else the first
                if (data.size() == 25) {
                    data = Lists.newArrayList(data);
                    if (data.get(24).isDataMissing() && !data.get(23).isDataMissing()) {
                        data.remove(24);
                    } else {
                        data.remove(0);
                    }
                }

                String historicalFileName = getRSSFilename(site, p);
                Path outputDir = getOutputDirectory();
                Path rssDir = outputDir.resolve("RSS");
                checkAndCreateDirectory(rssDir);
                Path siteDir = outputDir.resolve(getRSSDirectory(site));
                checkAndCreateDirectory(siteDir);
                Path historicalFile = siteDir.resolve(historicalFileName);
                printOut("Generating RSS file: "+historicalFile);
                
                try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(historicalFile))) {
                    pw.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
                    pw.append("\n");
                    pw.append("<rss version=\"2.0\">");
                    pw.append("\n");
                    pw.append("<channel>");
                    pw.append("\n");
                    pw.append("<title>" + site.getName() + "</title>");
                    pw.append("\n");

                    data.stream().forEach((datum) -> {
                        pw.append("<item>");
                        pw.append("\n");
                        pw.append("<title>" + TIME_FORMAT.print(datum.getLocalEndTime()) + "</title>");
                        pw.append("\n");
                        pw.append("<description>");
                            pw.append(TIME_FORMAT.print(datum.getLocalEndTime()));
                            pw.append("; ");
                            pw.append(p.getName());
                            pw.append("; ");
                            pw.append(String.valueOf(datum.getNowCastConcValueOnly()));
                            pw.append("; ");
                            pw.append(String.valueOf(datum.getNowCastAirIndex()));
                            pw.append("; ");
                            pw.append(catNameForCatID(datum.getNowCastAirIndexCategoryID()));
                        pw.append("</description>");
                        pw.append("\n");
                        pw.append("<Param>"+ p.getName() +"</Param>");
                        pw.append("\n");
                        pw.append("<Conc>"+ datum.getHourlyDataValue()+"</Conc>");
                        pw.append("\n");
                        pw.append("<NowCastConc>"+ datum.getNowCastConcValueOnly()+"</NowCastConc>");
                        pw.append("\n");
                        pw.append("<AQI>"+ datum.getNowCastAirIndex() +"</AQI>");
                        pw.append("\n");
                        pw.append("<Desc>" + datum.getNowCastAirIndexCategoryName() + "</Desc>");
                        pw.append("\n");
                        pw.append("<ReadingDateTime>");
                        pw.append(TIME_FORMAT.print(datum.getLocalEndTime()));
                        pw.append("</ReadingDateTime>");
                        pw.append("\n");
                        pw.append("</item>");
                        pw.append("\n");                       
                    });
                    pw.append("</channel>");
                    pw.append("\n");
                    pw.append("</rss>");
                    pw.flush();
                    pw.close();                     
                } catch (IOException e) {
                    printError("Error generating RSS for agency '"+agency.getName()+"' and site '"+site.getName()+"'.", e);
                }
            });
        });         
        
        return true;
    }    
    
    private static final DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    
    private static String catNameForCatID(int catID) {
        switch (catID) {
            case 1:
              return "Good";
            case 2:
              return "Moderate";
            case 3:
              return "Unhealthy for Sensitive Groups";
            case 4:
              return "Unhealthy";
            case 5:
              return "Very Unhealthy";
            case 6:
              return "Hazardous";
            case 0:
            default:
              return "";
        }
    }
    
    
}
