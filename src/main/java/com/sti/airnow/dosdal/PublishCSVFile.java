package com.sti.airnow.dosdal;

import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.sti.justice.Pair;
import com.sti.justice.dmc.model.AgencyMetaData;
import com.sti.justice.dms.enums.AQICategory;
import com.sti.justice.dms.model.Agency;
import com.sti.justice.dms.model.Parameter;
import com.sti.justice.dms.model.QcCode;
import com.sti.justice.dms.model.Site;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.kohsuke.args4j.Option;

import java.nio.file.Path;
import java.util.List;


public class PublishCSVFile extends AbstractDosPublisher
{
    public static void main(String[] args) {
        standardMain(args, PublishCSVFile.class);
    }
    
    @Option(name="-y",
            aliases={"-year"},
            usage="Generate a historical data file by year")
    private String historicalByYear;     
        
    @Option(name="-mtd",
            aliases={"-mtd-only"},
            usage="Generate a historical data file for the current month only.")
    private boolean mtdOnly = false;

    @Option(name="-ytd",
            aliases={"-ytd-only"},
            usage="Generate a historical data file for the current month only.")
    private boolean ytdOnly = false;

    @Option(name="-date",
            aliases={"-requestedDate"},
            usage="Generate a historical data file for this date."
    )
    private String requestedDate;

    @Override
    protected boolean validateArguments()
    {
        if (!super.validateArguments()) {
            return false;
        }
        
        if (!Strings.isNullOrEmpty(historicalByYear) && mtdOnly) {
            printError("Cannot specify both a year and the MTD only flag.");
            return false;
        }
        
        return true;
    }
    
    @Override
    protected boolean generateOutput()
    {        
        if(!Strings.isNullOrEmpty(historicalByYear)) {
            DateTime startDate = new DateTime(historicalByYear + "-01-01T00:00");
            DateTime endDate = new DateTime(historicalByYear + "-12-31T23:59");  
            generateFiles(startDate, endDate, historicalByYear + "_YTD");
        }
        // If no historical year is specified, generate data for the current year.
        // This is done in two files, one for the current month and the other for all
        // previous months in this year.  We may only generate one file, or none at
        // all on Jan 1.
        else {
            //DateTime currentDate = new DateTime("2016-08-31T23:59");
            DateTime currentDate = (Strings.isNullOrEmpty(requestedDate)) ? new DateTime() : DateTime.parse(requestedDate).dayOfMonth().withMaximumValue();
            if (mtdOnly) {
                printOut("Skipping YTD files due to presence of -mtd flag.");
            } else { //ytdonly
                if (currentDate.getMonthOfYear() > 1) {
                    DateTime startDate = currentDate.withDayOfYear(1).withTimeAtStartOfDay();
                    DateTime endDate = currentDate.withDayOfMonth(1).withTimeAtStartOfDay().minusMinutes(1);
                    generateFiles(startDate, endDate, String.valueOf(currentDate.getYear()) + "_YTD");
                } else {  //Generate the file for the previous year
                    DateTime startDate = new DateTime().dayOfYear().withMinimumValue().withTimeAtStartOfDay().minusYears(1);
                    DateTime endDate = new DateTime().dayOfYear().withMinimumValue().withTimeAtStartOfDay().minusMinutes(1);
                    generateFiles(startDate, endDate, String.valueOf(startDate.getYear()) + "_YTD");
                }
            }

            if(ytdOnly) {
                printOut("Skipping MTD files due to presence of -ytd flag.");
            }
            else { //mtdonly
                if (currentDate.getDayOfMonth() > 1) {
                    DateTime startDate = currentDate.withDayOfMonth(1).withTimeAtStartOfDay();
                    DateTime endDate = currentDate.withTimeAtStartOfDay().minusMinutes(1);
                    String key = String.valueOf(currentDate.getYear()) + "_" + currentDate.toString("MM") + "_MTD";
                    generateFiles(startDate, endDate, key);
                } else { //Generate the file for previous month
                    DateTime startDate = currentDate.minusMonths(1).withDayOfMonth(1).withTimeAtStartOfDay();
                    DateTime endDate = currentDate.dayOfMonth().withMinimumValue().withTimeAtStartOfDay().minusMinutes(1);
                    String key = String.valueOf(currentDate.getYear()) + "_" + startDate.toString("MM") + "_MTD";
                    generateFiles(startDate, endDate, key);
                }
            }
    }

        return true;
    }

    private static final String[] CSV_HEADERS = {
            "Site", "Parameter", "Date (LT)", "Year", "Month", "Day", "Hour", "NowCast Conc.", "AQI", "AQI Category",
            "Raw Conc.", "Conc. Unit", "Duration", "QC Name"
    };
    private static final int NUM_COLS = CSV_HEADERS.length;

    private static final DateTimeFormatter FULL_TIME_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm aa");
    private static final DateTimeFormatter YEAR_FORMAT = DateTimeFormat.forPattern("YYYY");
    private static final DateTimeFormatter MONTH_FORMAT = DateTimeFormat.forPattern("MM");
    private static final DateTimeFormatter DAY_FORMAT = DateTimeFormat.forPattern("dd");
    private static final DateTimeFormatter HOUR_FORMAT = DateTimeFormat.forPattern("HH");

    private void generateFiles(DateTime startDate, DateTime endDate, String fileKey)
    {
        final String durationName = durationDal.getById(HOURLY_DURATION).getName();
        final CsvWriterSettings writerSettings = new CsvWriterSettings();

        List<Pair<Agency,Site>> sites = retrieveAllSites();

        sites.stream().forEach((pair) -> {
//        Set<Integer> wantedSiteIds = ImmutableSet.of(25056);//24279,24281,24609,24610);
//
//        sites.stream()
//                    .filter(pair -> wantedSiteIds.contains(pair.getSecond().getSiteID()))
//                    .forEach((pair) -> {
            Agency agency = pair.getFirst();
            Site site = pair.getSecond();
            printTime("Starting site "+site.getName());
            final String siteName = sanitizeFilename(site.getName());
            
            AgencyMetaData dmcAgency = agencyMetaDataDal.getAgencyById(agency.getAgencyId());
            if (dmcAgency == null) {
                printOut("Warning: No DMC agency record found for agency ID "+agency.getAgencyId());
            }

            List<Integer> params = parameterDal.getPrimaryParameterIdsForSite(site.getSiteID());
         
            params.stream().forEach((pid) -> {
                Parameter parameter = parameterDal.getById(pid);
                printTime("Starting parameter "+parameter.getName());
                String paramName = sanitizeFilename(parameter.getIngestCode());
                //POC poc = pocDal.getMonitorPOC(site.getSiteID(), pid, HOURLY_DURATION);
                Integer possiblePoc = getPOC(site.getSiteID(), pid);
                if (possiblePoc == null) {
                    printError("Warning: Could not determine POC for site '"+site.getName()+"' and parameter '"+parameter.getDisplayName()+"'.");
                    return;
                }
                final int poc = possiblePoc;
                Integer nonPrimaryPOC = null;
                if (site.getIntlCode().equals("156BJ1010001")) {
                    nonPrimaryPOC = getNonPrimaryPOC(site.getSiteID(), pid);
                }

                int offsetMinutes = (int)(site.getUTCOffsetDouble() * 60);
                DateTime offsetStart = startDate.minusMinutes(offsetMinutes);
                DateTime offsetEnd = endDate.minusMinutes(offsetMinutes);
                printTime("Query start");
                List<com.sti.justice.dmc.model.SurrogateData> data = dmcDataDal.getSurrogatesForDateRange(site.getSiteID(), pid, poc, offsetStart, offsetEnd, HOURLY_DURATION, 150);
                //OLD DMS call - List<SurrogateData> data = dataDal.getSurrogatesForDateRange(site.getSiteID(), pid, poc, offsetStart, offsetEnd, HOURLY_DURATION, 150);

                //for Beijing we need to call data for the non-primary POC to fill in gaps in the primary POC's data range
                List<com.sti.justice.dmc.model.SurrogateData> nonPrimaryData = Lists.newArrayList();
                if (nonPrimaryPOC != null) { // !=null ??? !nonPrimaryPOC.equals(null)
                    nonPrimaryData = dmcDataDal.getSurrogatesForDateRange(site.getSiteID(), pid, nonPrimaryPOC, offsetStart, offsetEnd, HOURLY_DURATION, 150);
                }

                List<com.sti.justice.dmc.model.SurrogateData> finalData = Lists.newArrayList();

                if (nonPrimaryData != null) {
                    for (com.sti.justice.dmc.model.SurrogateData record : data) {
                        if (record.getNowCastAirIndex().equals(-999) && record.getHourlyDataValue().equals(-999.0)) {
                            for (com.sti.justice.dmc.model.SurrogateData nonPrimaryRecord : nonPrimaryData) {
                                if (record.getUtc().equals(nonPrimaryRecord.getUtc())) {
                                    com.sti.justice.dmc.model.SurrogateData.Builder builder = com.sti.justice.dmc.model.SurrogateData.builder();
                                    builder.siteId(record.getSiteId());
                                    builder.siteName(record.getSiteName());
                                    builder.parameterId(record.getParameterId());
                                    builder.parameterName(record.getParameterName());
                                    builder.unitName(record.getUnitName());
                                    builder.utc(record.getUtc());
                                    builder.lst(record.getLst());
                                    builder.localBeginTime(record.getLocalBeginTime());
                                    builder.localEndTime(record.getLocalEndTime());
                                    builder.dataMissing(nonPrimaryRecord.isDataMissing());
                                    builder.dataValid(nonPrimaryRecord.isDataValid());
                                    builder.dataQCCode(nonPrimaryRecord.getDataQCCode());
                                    builder.hourlyDataValue(nonPrimaryRecord.getHourlyDataValue());
                                    builder.nowCastConcValueOnly(nonPrimaryRecord.getNowCastConcValueOnly());
                                    builder.nowCastAirIndex(nonPrimaryRecord.getNowCastAirIndex());
                                    builder.nowCastAirIndexCategoryID(nonPrimaryRecord.getNowCastAirIndexCategoryID());
                                    finalData.add(builder.build());
                                    /*public SurrogateData build() {
                                    return new SurrogateData(siteId, siteName, parameterId, parameterName, unitName, utc, lst, localBeginTime,
                                    localEndTime, dataMissing, dataValid, dataQCCode, hourlyDataValue, nowCastConcValueOnly,
                                    nowCastAirIndex, nowCastAirIndexCategoryID);*/
                                }
                            }

                        } else {
                            finalData.add(record);
                        }
                    }
                } else {
                    finalData = data;
                }

                //now regardless of whether replacements were made from the non-primary POC or not, set data equal to the result, i.e., finalData
                data = finalData;


                printTime("Query end");
                int numLeadingMissingDatas = 0;
                for (com.sti.justice.dmc.model.SurrogateData datum : data) {
                    if (!datum.isDataMissing()) {
                        break;
                    }
                    numLeadingMissingDatas++;
                }
                if (numLeadingMissingDatas == data.size()) {
                    // no valid data.
                    printOut("All data was missing for site "+site.getName()+", skipping.");
                    return;
                }
                Path rootOutputDir = getOutputDirectory();
                Path historicalAgencyDir = rootOutputDir.resolve("historical");
                Path siteDir = historicalAgencyDir.resolve(siteName);
                Path yearDir = siteDir.resolve(YEAR_FORMAT.print(startDate));
                checkAndCreateDirectory(yearDir);
                String historicalFileName = siteName + "_" + paramName + "_" + fileKey + ".csv";
                Path outputFile = yearDir.resolve(historicalFileName);
                printOut("Generating CSV file: "+outputFile);

                CsvWriter writerToClose = null;
                try {
                    final CsvWriter writer = new CsvWriter(outputFile.toFile(), writerSettings);
                    writerToClose = writer;
                    writer.writeHeaders(CSV_HEADERS);

                    final List<String> values = Lists.newArrayListWithExpectedSize(NUM_COLS);

                    //data.stream().skip(numLeadingMissingDatas).forEach((datum) -> {
                    data.stream().forEach((datum) -> {
                        DateTime endTime = datum.getLocalEndTime().withZoneRetainFields(DateTimeZone.UTC);
                        values.add(site.getName());
                        values.add(parameter.getName());
                        values.add(FULL_TIME_FORMAT.print(endTime));
                        values.add(YEAR_FORMAT.print(endTime));
                        values.add(MONTH_FORMAT.print(endTime));
                        values.add(DAY_FORMAT.print(endTime));
                        values.add(HOUR_FORMAT.print(endTime));

                        values.add(datum.getNowCastConcValueOnly().toString());
                        values.add(datum.getNowCastAirIndex().toString());
                        Integer catId = datum.getNowCastAirIndexCategoryID();
                        values.add(catId == null ? "" : AQICategory.valueOf(catId).getLabel());
                        values.add(datum.getHourlyDataValue().toString()); //Raw Conc Value Scaled

                        values.add(parameter.getUnit());
                        values.add(durationName);
                        values.add(Strings.nullToEmpty(getQcCodeName(datum.getDataQCCode())));

                        if (values.size() != NUM_COLS) {
                            printError("Unexpected number of columns in row: "+values.size());
                        }

                        writer.writeRow(values.toArray());
                        values.clear();
                    });

                    printOut("Generated CSV " + site.getName());

                } catch (Exception ex) {
                    printError("Error generating file '"+outputFile+"'", ex);
                } finally {
                    if (writerToClose != null) {
                        writerToClose.close();
                    }
                }
            });
        });        
    }


    private final Cache<Integer, String> qcCodeCache = CacheBuilder.newBuilder().maximumSize(100).build();

    protected String getQcCodeName(Integer qcCodeId) {
        if (qcCodeId == null) {
            return "Missing";
        }
        String value = qcCodeCache.getIfPresent(qcCodeId);
        if (value != null) {
            return value;
        }
        QcCode qcCode = qcCodeDal.getByID(qcCodeId);
        if (qcCode != null) {
            value = qcCode.getName();
            qcCodeCache.put(qcCodeId, value);
        }
        return value == null ? "Missing" : value;
    }

    void setHistoricalByYear(String historicalByYear) {
        this.historicalByYear = historicalByYear;
    }

    void setMtdOnly(boolean mtdOnly) {
        this.mtdOnly = mtdOnly;
    }

    void setYtdOnly(boolean ytdOnly) {
        this.ytdOnly = ytdOnly;
    }

    void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }
}
