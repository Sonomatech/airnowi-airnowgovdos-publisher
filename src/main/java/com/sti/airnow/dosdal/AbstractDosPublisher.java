package com.sti.airnow.dosdal;

import com.sti.justice.Pair;
import com.sti.justice.cli.PublishDMSFileCommand;
import com.sti.justice.dms.enums.AgencyTypeEnum;
import com.sti.justice.dms.model.Agency;
import com.sti.justice.dms.model.Monitor;
import com.sti.justice.dms.model.POC;
import com.sti.justice.dms.model.Site;
import java.util.ArrayList;
import java.util.List;

/**
 * Another abstract base class, this one adds some DMS queries used by all DoS jobs.
 */
public abstract class AbstractDosPublisher extends PublishDMSFileCommand
{
    public static final int HOURLY_DURATION = 10; // DMS hardcoded value

    @Override
    protected String getApplicationName() {
        return "AirNowGov-GenFiles";
    }

    protected List<Pair<Agency,Site>> retrieveAllSites()
    {
        printOut("Retrieving all USDOS agencies (AgencyType=2).");
        List<Agency> agencies = agencyDal.getByType(AgencyTypeEnum.USDOS_Post);
        printOut("Found "+agencies.size()+" agencies.");
        List<Pair<Agency,Site>> sites = new ArrayList<>();
        
        agencies.stream().forEach((agency) -> {
            List<Site> agencySites = siteDal.getByAgencyID(agency.getAgencyId());
            if (agencySites == null || agencySites.size() < 1) {
                printError("Could not find any site for USDOS agency "+agency.getName());
                return;
            } else {
                printOut("Found "+agencySites.size()+" site(s) for agency "+agency.getName());
            }
            agencySites.stream().forEach((site) -> {
                Pair<Agency,Site> pair = Pair.of(agency, site);
                sites.add(pair);
            });
        });
        
        return sites;
    }
    
    protected Integer getPOC(int siteId, int parameterId)
    {
        try {
            POC pocObj = pocDal.getMonitorPOC(siteId, parameterId, HOURLY_DURATION);
            return pocObj.getPoc();
        } catch (Exception e) {
            List<Monitor> monitors = monitorDal.getBySiteID(siteId);
            for (Monitor monitor : monitors) {
                if (!monitor.getIsPrimary()) {
                    continue;
                }
                if (monitor.getDurationID() != HOURLY_DURATION) {
                    continue;
                }
                if (monitor.getParameterID() != parameterId) {
                    continue;
                }
                return monitor.getPOC();
            }
        }
        return null;
    }

    protected Integer getNonPrimaryPOC(int siteId, int parameterId)
    {
        try {
            POC pocObj = pocDal.getMonitorPOC(siteId, parameterId, HOURLY_DURATION);
            return pocObj.getPoc();
        } catch (Exception e) {
            List<Monitor> monitors = monitorDal.getBySiteID(siteId);
            for (Monitor monitor : monitors) {
                if (monitor.getIsPrimary()) {
                    continue;
                }
                if (monitor.getDurationID() != HOURLY_DURATION) {
                    continue;
                }
                if (monitor.getParameterID() != parameterId) {
                    continue;
                }
                return monitor.getPOC(); //will return the first non-primary POC we come across that
                                         //is hourly and for the correct parameter
            }
        }
        return null;
    }
}
