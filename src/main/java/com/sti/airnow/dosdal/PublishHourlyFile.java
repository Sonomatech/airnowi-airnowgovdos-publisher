package com.sti.airnow.dosdal;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.*;
import com.sti.justice.Pair;
import com.sti.justice.dmc.model.AgencyMetaData;
import com.sti.justice.dms.enums.PluginsEnum;
import com.sti.justice.dms.model.*;
import com.sti.justice.json.JsonUtil;
import com.sti.justice.util.Numbers;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.kohsuke.args4j.Option;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 */
public class PublishHourlyFile extends AbstractDosPublisher
{
    public static void main(String[] args) {
        standardMain(args, PublishHourlyFile.class);
    }

    public static final String DEFAULT_REMOTE_FILE_PATH = "http://dosairnowdata.org/dos/";
    public static final String DEFAULT_OUTPUT_FILENAME = "AllPosts24Hour.json";

    @Override
    protected String getDefaultOutputFilename() {
        return DEFAULT_OUTPUT_FILENAME;
    }

    @Option(name="-b",
            aliases={"-base", "-baseUrl", "-url"},
            usage="Specify the default base URL for RSS files links.")
    private String REMOTE_FILE_PATH = DEFAULT_REMOTE_FILE_PATH;

    /**
     * This implementation calls generateJson() and then writes the resulting
     * JSON to the output.
     * @param out
     * @return true if successful
     */
    @Override
    protected boolean generate(OutputStream out) {
        JsonObject root = generateJson();
        JsonUtil.writeJson(root, out);
        return true;
    }

    @Override
    protected boolean validateArguments()
    {
        if (!super.validateArguments()) {
            return false;
        }

        if (!REMOTE_FILE_PATH.endsWith("/")) {
            REMOTE_FILE_PATH = REMOTE_FILE_PATH + "/";
        }

        return true;
    }

    protected JsonObject generateJson()
    {
        JsonObject root = new JsonObject();
        List<Pair<Agency,Site>> sites = retrieveAllSites();
        
        sites.stream().forEach((pair) -> {
            Agency agency = pair.getFirst();
            Site site = pair.getSecond();
            try {
                JsonObject siteJson = genSiteJson(agency, site);
                if (siteJson != null) {
                    String name = site.getName();
                    root.add(name, siteJson);
                }
            } catch (Exception e) {
                printError("Error generating JSON for agency '"+agency.getName()+"' and site '"+site.getName()+"'.", e);
            }
        });
        
        return root;
    }
    
    private static final DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss aa");

    private JsonObject genSiteJson(Agency agency, Site site)
    {
        final int siteID = site.getSiteID();
        JsonObject siteObj = new JsonObject();
        
        // First check the DMC agency record.
        AgencyMetaData dmcAgency = agencyMetaDataDal.getAgencyById(agency.getAgencyId());
        final boolean includeMonitorData;
        final String agencyMessage;
        if (dmcAgency == null) {
            printOut("Warning: No DMC agency record found for agency ID "+agency.getAgencyId());
            return null;
        } else {            
            includeMonitorData = dmcAgency.getShowOnAirNowGov();
            agencyMessage = dmcAgency.getAirNowGovMessage();
            if (!includeMonitorData && Strings.isNullOrEmpty(agencyMessage)) {
                printOut("'ShowOnAirnowGov' is false and no message defined so skipping site: "+site.getName());
                return null;
            }
        }

        // Check to see if site is active
        if (site.getIsActive()) {
            // Coords in Long-Lat
            JsonArray coords = new JsonArray();
            coords.add(new JsonPrimitive(site.getLongitude()));
            coords.add(new JsonPrimitive(site.getLatitude()));
            siteObj.add("coordinates", coords);

            Region curRegion = regionDal.getById(agency.getRegionId());
            siteObj.add("mission", new JsonPrimitive(curRegion.getName()));

            JsonArray monitors = new JsonArray();
            if (includeMonitorData) {
                List<Integer> params = parameterDal.getPrimaryParameterIdsForSite(siteID);
                printOut("Found "+params.size()+" primary parameters for site "+site.getName());
                params.stream().forEach((pid) -> {
                    JsonObject monitor = generateOneMonitor(agency, site, pid);
                    if (monitor != null) {
                        monitors.add(monitor);
                    } else {
                        printError("Warning: no monitor data generated, so skipping parameter ID "+pid+" for site "+site.getName());
                    }
                });
            }

            printOut("Generated data for "+monitors.size()+" monitors for site "+site.getName());
            siteObj.add("monitors", monitors);

            if (!Strings.isNullOrEmpty(agencyMessage)) {
                siteObj.add("message", new JsonPrimitive(agencyMessage));
            }

            siteObj.add("displayOnWebsite", new JsonPrimitive(includeMonitorData));
        } else {
            printOut("Site " + site.getName() + " is InActive. Skipping.");
        }

        return siteObj;
    }
    
    protected JsonObject generateOneMonitor(Agency agency, Site site, int paramId)
    {
        final int agencyId = agency.getAgencyId();
        final int siteId = site.getSiteID();
        final Parameter p = parameterDal.getById(paramId);
        Integer possiblePoc = getPOC(site.getSiteID(), paramId);
        if (possiblePoc == null) {
            printError("Warning: Could not determine POC for site '"+site.getName()+"' and parameter '"+p.getDisplayName()+"'.");
            return null;
        }
        final int poc = possiblePoc;
        //final POC poc = pocDal.getMonitorPOC(site.getSiteID(), paramId, HOURLY_DURATION);
        //DateTime start = DateTime.now(DateTimeZone.UTC).minusHours(25);
        List<SurrogateData> data = dataDal.getHourlySurrogates(site.getSiteID(), paramId, poc, -24);
        if (data.size() != 25) {
            printOut("Warning: data request for site '"+site.getName()+"' and parameter '"+p.getDisplayName()+"' only returned "+data.size()+" results.");
        } else {
            printOut("Found 25 hourly values for site '"+site.getName()+"' and parameter '"+p.getDisplayName()+"'.");
        }


        if (data.isEmpty()) {
            // Skip monitors with no data at all
            printError("No data found for site '"+site.getName()+"' and parameter '"+p.getDisplayName()+"', skipping.");
            return null;
        } else {
            DateTimeComparator datecomp = DateTimeComparator.getInstance();
            try {
                // Sort by time
                data = data.stream().sorted((d1, d2) -> datecomp.compare(d1.getLocalEndTime(), d2.getLocalEndTime())).collect(Collectors.toList());
            } catch (Exception e) {
                printError("Error sorting by date for agency '"+agency.getName()+"' and site '"+site.getName()+"'.", e);
            }
            // Now remove the last element (if the last value is missing, but the previous one is not) else the first
            if (data.size() == 25) {
                data = Lists.newArrayList(data);
                if (data.get(24).isDataMissing() && !data.get(23).isDataMissing()) {
                    data.remove(24);
                } else {
                    data.remove(0);
                }
            }
            // fill in missing values
            Map<DateTime, SurrogateData> dataByDateTime = new HashMap<>();
            DateTime subjectDateTime = data.get(0).getLst();
            DateTime endDateTime = data.get(data.size()-4).getLst();
            while(!subjectDateTime.isAfter(endDateTime)) {
                dataByDateTime.put(subjectDateTime, null);
                subjectDateTime = subjectDateTime.plusHours(1);
            }
            data.forEach(d -> dataByDateTime.put(d.getLst(), d));
            List<SurrogateData> dataToAdd = dataByDateTime.entrySet().stream()
                    .filter(e -> e.getValue() == null)
                    .map(e -> SurrogateData.builder()
                            .lst(e.getKey())
                            .localBeginTime(e.getKey())
                            .localEndTime(e.getKey().plusHours(1))
                            .dataMissing(true).build())
                    .collect(Collectors.toList());
            data.addAll(dataToAdd);
            data = data.stream().sorted((d1, d2) -> datecomp.compare(d1.getLocalEndTime(), d2.getLocalEndTime())).collect(Collectors.toList());

            long numMissing = data.stream().filter((datum) -> datum.isDataMissing()).count();
            long numValid = data.stream().filter((datum) -> datum.isDataValid()).count();
            printOut("There are "+numMissing+" missing values and "+numValid+" valid entries.");
        }
        final JsonObject monitor = new JsonObject();
        monitor.add("parameter", new JsonPrimitive(p.getIngestCode()));
        SurrogateData first = data.get(0);
        DateTime endTime = first.getLocalEndTime();
        String timeStr = TIME_FORMAT.print(endTime);
        // TODO: rename this field.
        monitor.add("beginTimeLT", new JsonPrimitive(timeStr));
        String unit = first.getUnitName();
        if (unit.equals("UG/M3")) {
            unit = "ug/m3";
        }
        monitor.add("concUnit", new JsonPrimitive(unit));
        JsonArray rawHourlyConcentrationValues = new JsonArray();
        JsonArray aqis = new JsonArray();
        JsonArray aqiCats = new JsonArray();
        data.stream().forEach((datum) -> {
            if (datum.isDataMissing() || !datum.isDataValid()) {
                rawHourlyConcentrationValues.add(new JsonNull());
            } else {
                String rawHourlyConcentrationValue = Numbers.makeFixed(datum.getHourlyDataValue(), 0, 5, false);
                rawHourlyConcentrationValues.add(new JsonPrimitive(Double.parseDouble(rawHourlyConcentrationValue)));
            }
            if (datum.getNowCastAirIndex() == null || datum.getNowCastAirIndex() == -999) {
                aqis.add(new JsonNull());
                aqiCats.add(new JsonNull());
            } else {
                aqis.add(new JsonPrimitive(datum.getNowCastAirIndex()));
                aqiCats.add(new JsonPrimitive(datum.getNowCastAirIndexCategoryID()));
            }
        });
        monitor.add("aqi", aqis);
        monitor.add("aqiCat", aqiCats);
        monitor.add("conc", rawHourlyConcentrationValues);
        
        String rssPath = REMOTE_FILE_PATH + PublishRSSFile.getRSSPath(site, p);
        monitor.addProperty("rss", rssPath);
        
        // Twitter handle
        pluginAccountDal.getByAgencyID(agencyId).stream()
            .filter((acct) -> PluginsEnum.SocialMedia.hasId(acct.getPluginID()))
            .forEach((acct) -> {
                // Default to the first handle we find.  This might be wrong.
                if (!monitor.has("twitter")) {
                    printOut("Found twitter handle '"+acct.getName()+"' for agency ID "+
                                agencyId + " Enabled: " + acct.getEnabled());
                    setTwitterHandle(monitor, acct.getName(), acct.getEnabled());
                }
                // Now check each account parameter to see if it matches ours.
                // TODO: probably what we really need is a stored proc to do all this.
                pluginAccountParameterDal.getByAccountID(acct.getAccountID()).stream()
                    .filter((pap) -> (pap.getParameterID() == paramId && pap.getPoc() == poc && pap.getPluginAccountSiteID() == siteId))
                    .forEach((pap) -> {
                        // We have a match.
                        String handle = acct.getName();
   
                        // Note that this may repeat, matching more than one element, which is why
                        // we log here (more than one match is not a big problem).
                        printOut("Found twitter handle '"+handle+"' for agency ID "+
                                agencyId +", parameter="+p.getIngestCode()+" and POC "+poc + "Enabled: " + acct.getEnabled());
                        setTwitterHandle(monitor, acct.getName(), acct.getEnabled());
                    });
            });
        
        return monitor;
    }
    
    static final void setTwitterHandle(JsonObject obj, String handle, Boolean enabled)
    {
        if (enabled) {
            String fullHandle = "http://www.twitter.com/" + handle;
            obj.add("twitter", new JsonPrimitive(fullHandle));
        }
    }

    public List<PluginAccount> getByAgencyID(int id) {
        return pluginAccountDal.getByAgencyID(id);
    }
}
