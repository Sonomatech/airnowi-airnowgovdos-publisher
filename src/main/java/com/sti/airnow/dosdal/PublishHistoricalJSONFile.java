package com.sti.airnow.dosdal;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sti.justice.cli.PublishDMSFileCommand;
import com.sti.justice.json.JsonUtil;
import org.joda.time.DateTime;
import org.kohsuke.args4j.Option;

import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

public class PublishHistoricalJSONFile extends PublishDMSFileCommand
{
    public static void main(String[] args) {
        standardMain(args, PublishHistoricalJSONFile.class);
    }

    // TODO: both of these should be configurable.
    public static final String DEFAULT_OUTPUT_FILENAME = "AllPostsHistorical.json";
    public static final String DEFAULT_REMOTE_FILE_PATH = "http://dosairnowdata.org/dos/";

    @Option(name="-b",
            aliases={"-base", "-baseUrl", "-url"},
            usage="Specify the default base URL for historical files links.")
    private String urlPrefix = DEFAULT_REMOTE_FILE_PATH;

    private static final String THIS_YEAR = String.valueOf(DateTime.now().getYear());
   
    @Override
    protected boolean validateArguments()
    {
        if (!super.validateArguments()) {
            return false;
        }

        if (!urlPrefix.endsWith("/")) {
            urlPrefix = urlPrefix + "/";
        }

        return true;
    }

    @Override
    protected String getDefaultOutputFilename() {
        return DEFAULT_OUTPUT_FILENAME;
    }
    
    /**
     * This implementation calls generateJson() and then writes the resulting
     * JSON to the output.
     * @param out
     * @return true if successful
     */
    @Override
    protected boolean generate(OutputStream out) {
        JsonObject root = generateJson();
        JsonUtil.writeJson(root, out);
        return true;
    }

    protected JsonObject generateJson()
    {
        printOut("Generating historical JSON with base URL "+urlPrefix);

        JsonObject root = new JsonObject();        
        // Get a list of everything in historical
        final Path outputDir = getOutputDirectory();
        final Path dir = outputDir.resolve("historical");
        final Stream<Path> directoryListing = listFiles(dir);
        directoryListing.forEach((Path agencyDir) ->
        {
            final String agencyName = agencyDir.getFileName().toString();
            final Stream<Path> agencyListing = listFiles(agencyDir);
            JsonObject monitorObj = new JsonObject();
            JsonArray monitorArr = new JsonArray();
            JsonArray fileArr = new JsonArray();
            JsonObject filesObj = new JsonObject();
            // Iterate through agency names in the historical directory
            JsonObject files = new JsonObject();  
            agencyListing.forEach((Path yearDir) -> {
                final String curYear = yearDir.getFileName().toString();
                final Stream<Path> yearListing = listFiles(yearDir);

                // Iterate through files in the year directory
                yearListing.forEach((Path agencyFile) -> {
                    String filename = agencyFile.getFileName().toString();
                    if (!filename.toLowerCase().endsWith(".csv")) {
                        printError("Found non-CSV file in historical directory: "+agencyFile.toAbsolutePath());
                        return;
                    }
                    String[] parts = Iterables.toArray(Splitter.on('_').split(filename), String.class);
                    if (parts == null || parts.length < 4 || parts.length > 5) {
                        printError("Unexpected filename in historical directory: "+filename);
                        return;
                    }

                    String propKey = curYear + ' ' + parts[1];
                    if (filename.contains("YTD")) {
                        if (filename.contains(THIS_YEAR)) {
                            propKey += " YTD";
                        }
                    } else if (filename.contains("MTD")) {
                        propKey += " MTD";

                        Integer currentMtdMonth = null;
                        JsonElement currentMtdFilename = files.get(propKey);
                        currentMtdMonth = (currentMtdFilename != null) ? Integer.parseInt(Iterables.toArray(Splitter.on('_').split(currentMtdFilename.toString()), String.class)[3]) : null;

                        Integer thisMtdMonth = Integer.parseInt(Iterables.toArray(Splitter.on('_').split(filename), String.class)[3]);

                        if(currentMtdFilename != null && currentMtdMonth > thisMtdMonth ) {  //check if this file is a later month
                            return;
                        }
                    } else {
                        printError("Unexpected filename in historical directory: "+filename);
                        return;
                    }

                    String path = urlPrefix + "historical/" + agencyName + "/" + curYear + "/" + filename;
                    files.addProperty(propKey, path);
                });
                fileArr.add(files);
                filesObj.add("files", files);
            });

            monitorArr.add(filesObj);
            monitorObj.add("monitors", monitorArr);
            root.add(agencyName, monitorObj);
        });
        
        return root;
    }
}
