package com.sti.airnow.dosdal;

import com.sti.justice.Pair;
import com.sti.justice.dms.model.Agency;
import com.sti.justice.dms.model.PluginAccount;
import com.sti.justice.dms.model.Site;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;


public class DmsTest
{
    @Test
    public void testDmsConnection() throws Exception
    {
        PublishHourlyFile job = new PublishHourlyFile();
        job.connectToDMS();
        job.dispose();
    }

    @Test
    public void testDmcConnection() throws Exception
    {
        PublishHourlyFile job = new PublishHourlyFile();
        job.connectToDMC();
        job.dispose();
    }
    
    @Test
    public void testTwitterStuff() throws Exception
    {
        PublishHourlyFile job = new PublishHourlyFile();
        assertTrue(job.initialize());
        List<Pair<Agency,Site>> sites = job.retrieveAllSites();
        System.out.println("Retrieved "+sites.size()+ " sites from PublishHourlyFile job.");
        sites.stream().forEach((pair) -> {
            Agency agency = pair.getFirst();
            Site site = pair.getSecond();
            List<PluginAccount> accts = job.getByAgencyID(agency.getAgencyId());
            System.out.println("Retrieved "+accts.size()+ " plugin accounts for site "+site.getDisplayName());
            accts.stream().forEach((acct) -> {
                System.out.println("PluginAccount key="+ acct.getUniqueKey()+ ", name="+acct.getName());
            });
        });
        
        job.dispose();
    }

    
}
